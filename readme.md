# Create user through REST api

Install and activate plugin


## header example
```
POST http://example.com/wp-json/userhandler/v1/create/

content-type : application/json
```
## request example

```
{
"api_key":"your api key here",
"reset_user": true, // optional
"user_email": "tester@gmail.com",
"first_name": "cornelius", // optional
"last_name": "vanderbild" // optional
}
```