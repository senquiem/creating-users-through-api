<?php
/*
Plugin Name: Creating users through API
Description: This plugin is creating new users from your requests ti own api
Author: Yurii Poliakov
Version: 1.0.3
*/

/*
- configuration page
-- public api key generate
-- public api key display/store

- create user from api
-- create new user's recovery url

*/




/*
* create menu configuration
*/

add_action('admin_menu', 'user_create_api_public_key_display');

// action function for above hook
function user_create_api_public_key_display() {
$type_of_publication = 'all';
 add_menu_page("Dashboard of testusBlank", 
               'User REST api key' ,
                'administrator',  
                'user_create_api_public_key_options_page',  
                'user_create_api_public_key_options_page');


 //call register settings function
  add_action( 'admin_init', 'WCorder_testusBlank_plugin_settings' );
}
/*
* register extra fields in console
*/

function WCorder_testusBlank_plugin_settings() {
  //register our settings
  register_setting( 'user_create_api_public_key', 'public_key' );
}

/**
* page views
*/

function user_create_api_public_key_options_page(){
?>
<div class="wrap testusBlank-for-orders">
<h1><?php echo __("Api public key") ?></h1>


<form method="post" action="options.php">
    <?php settings_fields( 'user_create_api_public_key' ); ?>
    <?php do_settings_sections( 'user_create_api_public_key' ); ?>
<div class="row public_key" >
<label for="public_key"><?php echo __("Your public key") ?></label>
<input type="text" readonly="readonly" id="public_key_show" name="public_key_show" value="<?= get_option("public_key") ? get_option("public_key") : "Generate your api key please"; ?>" size="32" />
<input type="hidden" id="public_key" name="public_key" value="<?php echo md5(rand(10,100)); ?>">

</div>
    
    <?php submit_button('generate new key'); ?>

</form>

</div>
<?php
}



/*
* api key generator
*/


// function for API answer

function create_user_from_api_data($data) {

if(get_option("public_key") != $data['api_key']){
  header('Content-Type: application/json');
  
  $responce['success'] = false;
  $responce['message'] = "Check your api key please";
  return $responce;
}
  

  $userdata = array(
    'user_email' => $data['user_email'],
    'user_login' => $data['user_email'],
    // this is required field...
    'user_pass' => wp_generate_password(16),
    'first_name' => $data['first_name'],
  'last_name' => $data['last_name'],
  );

    $responce = array();

  $existUserID = email_exists($data['user_email']);

  
  // check exist user
    if ( !empty( $existUserID ) ) {
      $responce['new_user'] = false;
        if($data['reset_user'] == true){
          $responce['recovery_url'] = api_send_password_reset_mail($existUserID);
        }
    } else
    {
      $userID = wp_insert_user( $userdata );
      $responce['recovery_url'] = api_send_password_reset_mail($userID);
      $responce['new_user'] = true;
    }
  
  if ( empty( $existUserID ) && empty( $userID ) ) {
      $responce['success'] = false;
  } else {
      $responce['success'] = true;
  
  }

  header('Content-Type: application/json');
  //echo json_encode($responce);
  return $responce;

}


// set basic password
add_action( 'login_header', 'wpb_login_logo' );

function wpb_login_logo() { ?>
    <style type="text/css">
        .pw-weak{
          display: none!important;
        }
    </style>
<script type="text/javascript">
  document.addEventListener("DOMContentLoaded", function(event) { 
  document.getElementById('pass1').value = '123456Aa';
  document.getElementById('pass1-text').value = '123456Aa';
  
  document.getElementsByClassName('password-input-wrapper')[0].className += ' show-password';

  document.querySelector('input[name="pw_weak"]').checked = true;

  console.log('good connection');
});
</script>


<?php }

// login user
add_action( 'password_reset', function( $user, $pass ) 
{
    if ( !is_wp_error( $user ) )
{
    wp_clear_auth_cookie();
    wp_set_current_user ( $user->ID );
    wp_set_auth_cookie  ( $user->ID );

    $redirect_to = user_admin_url();
    wp_safe_redirect( $redirect_to );
    exit();
}

}, 10, 2 );



// generate recovery link

function api_send_password_reset_mail($user_id){

    $user = get_user_by('id', $user_id);
    $user_login = $user->user_login;
    $recovery_key = get_password_reset_key( $user );
  $rp_link = network_site_url( "wp-login.php?action=rp&key=$recovery_key&login=" . rawurlencode( $user_login ), 'login' );

    return $rp_link;
}


add_action( 'rest_api_init', function () {
  register_rest_route( 'userhandler/v1', '/create/', array(
    'methods' => 'POST',
    'callback' => 'create_user_from_api_data'

  ) );
} );


if(!function_exists('wp_get_current_user')) {
    include(ABSPATH . "wp-includes/pluggable.php");
}


if ( current_user_can( 'manage_options' ) ) {
 add_filter('show_admin_bar', '__return_false');
}

add_action( 'load-profile.php', function() {
    if( ! current_user_can( 'manage_options' ) )
    exit( wp_safe_redirect( home_url('/plans/') ) );
} );